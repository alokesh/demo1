export interface MarkupGridModel {
  productType: string;
  itemPriceId: number;
  markup: string;
  unit: string;
  markupType: number;
  effectiveDate: string;
  expirationDate: string;
};

export interface MarkupGridDetails {
  markupId: number;
  markupName: string;
  markupList: MarkupGridModel[];
  expireLower: boolean;
  isCompleted: boolean;
};

export interface SaveMarkupGridModel {
  contractPriceProfileSeq: number;
  markupWrapper: MarkupGridDetails;
};

export interface ExceptionModel {
  contractPriceProfileSeq: number;
  exceptionName: string;
};

export interface SaveMarkupOnSellModel {
  contractPriceProfileSeq: number;
  effectiveDate: string;
  expirationDate: string;
  markupOnSell: boolean;
};

export interface RenameMarkupModel {
  contractPriceProfileSeq: number;
  markupId: string;
  markupName: string;
  newMarkupName: string;
}
